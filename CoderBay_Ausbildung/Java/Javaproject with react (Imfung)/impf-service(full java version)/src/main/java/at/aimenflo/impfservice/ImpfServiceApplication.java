package at.aimenflo.impfservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ImpfServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ImpfServiceApplication.class, args);
	}

}

package at.aimenflo.impfservice.entity;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "impfstoff")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id_impfstoff")
public class Impfstoff {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id_impfstoff;

    @OneToOne()
    @JoinColumn(name = "id_buchung", referencedColumnName = "id_buchung")
    private Buchung buchung;

    @Enumerated(EnumType.STRING)
    private ImpfstoffEnum impfstoff;

//--------------------------------CONSTRUCTOR START--------------------------------
    public Impfstoff(){

    }

    public Impfstoff(Integer id_impfstoff, ImpfstoffEnum impfstoff) {
        this.id_impfstoff = id_impfstoff;
        this.impfstoff = impfstoff;
    }
//--------------------------------CONSTRUCTOR END--------------------------------

//--------------------------------GET/SET START--------------------------------
    public Integer getId_impfstoff() {
        return id_impfstoff;
    }

    public void setId_impfstoff(Integer id_impfstoff) {
        this.id_impfstoff = id_impfstoff;
    }

    public ImpfstoffEnum getImpfstoff() {
        return impfstoff;
    }

    public void setImpfstoff(ImpfstoffEnum impfstoff) {
        this.impfstoff = impfstoff;
    }

    public Buchung getBuchung(){
        return buchung;
    }

    public void setBuchung(Buchung buchung){
        this.buchung = buchung;
    }
//--------------------------------GET/SET END--------------------------------
}

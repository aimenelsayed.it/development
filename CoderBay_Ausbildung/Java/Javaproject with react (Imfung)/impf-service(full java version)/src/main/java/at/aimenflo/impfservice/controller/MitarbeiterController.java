package at.aimenflo.impfservice.controller;

import at.aimenflo.impfservice.entity.Mitarbeiter;
import at.aimenflo.impfservice.repository.MitarbeiterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class MitarbeiterController {
    
    @Autowired
    MitarbeiterRepository mitarbeiterRepository;

//--------------------------------GET ALL MITARBEITER FROM DB START--------------------------------
    @RequestMapping(
        method = RequestMethod.GET,
        path = "/mitarbeiter",
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    public List<Mitarbeiter> getMitarbeiter(){

        List<Mitarbeiter> mitarbeiterList  = mitarbeiterRepository.findAll();
        return mitarbeiterList;
    }
//--------------------------------GET ALL MITARBEITER FROM DB END--------------------------------

//--------------------------------GET ONE MITARBEITER BY ID START--------------------------------
    @RequestMapping(
        method = RequestMethod.GET,
        path = "/mitarbeiter/{id_mitarbeiter}",
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Mitarbeiter getMitarbeiterById(@PathVariable Integer id_mitarbeiter){

        Optional<Mitarbeiter> mitarbeiter  = mitarbeiterRepository.findById(id_mitarbeiter);
        if (mitarbeiter.isPresent()) {
            return mitarbeiter.get();    
        }
        return null;
    }
//--------------------------------GET ONE MITARBEITER BY ID END--------------------------------

    //TODO Login mit session
}

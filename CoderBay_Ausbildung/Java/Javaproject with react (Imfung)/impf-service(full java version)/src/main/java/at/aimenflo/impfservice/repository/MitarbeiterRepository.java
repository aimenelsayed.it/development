package at.aimenflo.impfservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import at.aimenflo.impfservice.entity.Mitarbeiter;

public interface MitarbeiterRepository extends JpaRepository<Mitarbeiter, Integer>{
    Mitarbeiter findByEmail(String email);
}

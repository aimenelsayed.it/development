package at.aimenflo.impfservice;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import at.aimenflo.impfservice.controller.PatientController;
import at.aimenflo.impfservice.entity.GeschlechtEnum;
import at.aimenflo.impfservice.entity.Patient;

@SpringBootTest
@TestPropertySource(locations = "classpath:application-integrationtest.properties")
@TestMethodOrder(OrderAnnotation.class)
public class PatientControllerTests {
    
    @Autowired
    PatientController patientController;

    //TODO Buchung anlegen

    @Test
    @Order(1)
    void addPatientTest() throws ParseException {
        DateFormat df = new SimpleDateFormat("DD.MM.YYYY");
        Date gebDate = df.parse("02.01.1990");
        Patient patient = new Patient("Max", "Mustermann", "max.mustermann@test.at", "+43 00 11 22 334", gebDate, null, "+43 00 11 22 334", "Musterstrasse", "10", "2", "A5", "1010", "Wien", GeschlechtEnum.D);
        Patient patientdb = patientController.addPatient("A123789456", patient);
        assertEquals("A123789456", patientdb.getSvnr());
    }

    @Test
    @Order(2)
    void getPatientTest(){
        List<Patient> patientendb = patientController.getPatient();
        assertNotNull(patientendb);
        assertEquals(1, patientendb.size());
        Patient patientdb = patientendb.get(0);
        assertEquals("A123789456", patientdb.getSvnr());
    }
    
    @Test
    @Order(3)
    void getPatientBySvnrTest(){
        Patient patientdb = patientController.getPatientBySvnr("A123789456");
        assertNotNull(patientdb);
        assertEquals("A123789456", patientdb.getSvnr());
    }

    @Test
    @Order(4)
    void getPatientByNachnameVornameTest(){
        List<Patient> patientendb = patientController.getPatientByNachnameVorname("Mustermann", "Max");
        assertNotNull(patientendb);
        assertEquals(1, patientendb.size());
        Patient patientdb = patientendb.get(0);
        assertEquals("Mustermann", patientdb.getNachname());
        assertEquals("Max", patientdb.getVorname());
    }
}

package at.aimenflo.impfservice.entity;

import com.sun.istack.NotNull;
//import org.springframework.data.annotation.Id;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "infokanal")
public class Infokanal {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_infokanal", nullable=false)
    private Integer id_infokanal;

    @NotNull
    private boolean infokanalMail, infokanalSMS, infokanalTel;

//--------------------------------conssturactor------------------------------
    public Infokanal(){

    }

    public Infokanal(Integer id_infokanal, boolean infokanalMail, boolean infokanalSMS, boolean infokanalTel) {
        this.id_infokanal = id_infokanal;
        this.infokanalMail = infokanalMail;
        this.infokanalSMS = infokanalSMS;
        this.infokanalTel = infokanalTel;
    }

//-------------------------------Get and set ------------------------------
    public Integer getId_infokanal() {
        return id_infokanal;
    }

    public void setId_infokanal(Integer id_infokanal) {
        this.id_infokanal = id_infokanal;
    }

    public boolean getInfokanalMail() {
        return infokanalMail;
    }

    public void setInfokanalMail(boolean infokanalMail) {
        this.infokanalMail = infokanalMail;
    }

    public boolean getInfokanalSMS() {
        return infokanalSMS;
    }

    public void setInfokanalSMS(boolean infokanalSMS) {
        this.infokanalSMS = infokanalSMS;
    }

    public boolean getInfokanalTel() {
        return infokanalTel;
    }

    public void setInfokanalTel(boolean infokanalTel) {
        this.infokanalTel = infokanalTel;
    }
}

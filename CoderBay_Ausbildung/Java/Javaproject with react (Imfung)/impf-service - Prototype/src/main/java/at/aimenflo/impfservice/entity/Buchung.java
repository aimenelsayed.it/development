package at.aimenflo.impfservice.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.istack.NotNull;

import javax.persistence.*;
import java.sql.Date;
import java.time.LocalDateTime;

@Entity
@Table(name ="buchung")
public class Buchung {



    @Id
    //@GeneratedValue(strategy = GenerationType.AUTO.IDENTITY)
    private Integer id_buchung;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "svnr", referencedColumnName = "svnr")
    private Patient patient;

    //@NotNull
    @Column(nullable = false, columnDefinition = "TINYINT(1)")
    private boolean frei;

    @NotNull
    private String ort;

    @NotNull
    @Column(name = "termindatum")
    private LocalDateTime terminDatum;
 // --------------------------

    public Buchung() {

    }


    //-----------------------constractor------------------------------------------
    public Buchung(Integer id_buchung, Boolean frei, String ort, LocalDateTime terminDatum) {
        this.id_buchung = id_buchung;
        this.frei = frei;
        this.ort = ort;
        this.terminDatum = terminDatum;
    }


    //-----------------------------set and get ---------------------------------------

    public Integer getId_buchung() {
        return id_buchung;
    }

    public void setId_buchung(Integer id_buchung) {
        this.id_buchung = id_buchung;
    }

    public Boolean getFrei() {
        return frei;
    }

    public void setFrei(Boolean frei) {
        this.frei = frei;
    }

    public String getOrt() {
        return ort;
    }

    public void setOrt(String ort) {
        this.ort = ort;
    }

    public LocalDateTime getTerminDatum() {
        return terminDatum;
    }

    public void setTerminDatum(LocalDateTime terminDatum) {
        this.terminDatum = terminDatum;
    }

    @JsonIgnore
    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

}

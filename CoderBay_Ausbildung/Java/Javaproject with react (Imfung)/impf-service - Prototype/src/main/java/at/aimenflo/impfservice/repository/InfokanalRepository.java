package at.aimenflo.impfservice.repository;

import at.aimenflo.impfservice.entity.Infokanal;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InfokanalRepository extends JpaRepository<Infokanal, Integer> {



}

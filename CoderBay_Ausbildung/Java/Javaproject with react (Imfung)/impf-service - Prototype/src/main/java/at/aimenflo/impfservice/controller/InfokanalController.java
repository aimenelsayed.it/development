package at.aimenflo.impfservice.controller;


import at.aimenflo.impfservice.entity.Infokanal;
import at.aimenflo.impfservice.repository.InfokanalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class InfokanalController {

    @Autowired
    InfokanalRepository infokanalRepository;

    @RequestMapping(
            method = RequestMethod.GET,
            path = "/infokanal",
            produces = MediaType.APPLICATION_JSON_VALUE
    )

   public List<Infokanal> getInfokanal() {

        List<Infokanal> infokanalList = infokanalRepository.findAll();
        return  infokanalList;



    }


}

import { message, notification } from 'antd';
import { withSuccess } from 'antd/lib/modal/confirm';
import { description } from 'commander';

const openNotificationWithIcon = (type, message, description) => {
    notification[type]({
      message,
      description,
    });
  };

  export const  successNotification = (message, description) => 
  openNotificationWithIcon('sucess', message, description);

  export const infosNotification = (message, description) => 
  openNotificationWithIcon('info', message, description);

  export const warningNotification = (message, description) => 
  openNotificationWithIcon('warning', message, description);

   export const errorNotification = (message, description) => 
  openNotificationWithIcon('error', message, description);


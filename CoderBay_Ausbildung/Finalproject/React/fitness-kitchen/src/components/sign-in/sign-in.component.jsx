import React from 'react';
import './sign-in.styles.scss';
import FormInput from '../form-input/form-input.component';
import CustomButton from '../custom-button/custom-button.component';
import { googleSignInStart, emailSignInStart } from '../../redux/user/user.actions';
import { connect } from 'react-redux';




class SignIn extends React.Component {
constructor(props){
super(props);

this.state = {
    email: '',
    password: ''
};
}

handleSubmit = async event => {


    ///>>>>>>  destruct    to not write alot of state
    event.preventDefault(); 
    const { emailSignInStart } = this.props; 
    
    const{ email, password} = this.state;

    emailSignInStart(email, password);
    
    };




   



handleChnage = event => {
    const { value, name } = event.target;
    this.setState({ [name] : value})
};





////>>>>>>>>>>          signin and google signin
render() {
    const { googleSignInStart } = this.props;
    return(
        <div className='sign-in'>
            <h2>I already have an account</h2>
            <span>Sign in with your email and password</span>

            <form onSubmit={this.handleSubmit}>
                <FormInput name="email" value={this.state.email}  type="email" handleChange={this.handleChnage} required label="email" />
                
                <FormInput name="password" value={this.state.password}  type="password" handleChange={this.handleChnage} required label="password" />
                
                <div className='buttons'>
                <CustomButton type="submit From"  >Sign IN</CustomButton>
                <CustomButton type='button' onClick={googleSignInStart} isGoogleSignIn value="Submit Form"  >Sign in with Google </CustomButton>
                </div>
              
                
            </form>

        </div>
    )

    }

}

const mapDispatchToProps = dispatch => ({
    googleSignInStart: () => dispatch(googleSignInStart()),
    emailSignInStart: (email, password) => dispatch(emailSignInStart({email, password}))
});

 export default connect(null, mapDispatchToProps)(SignIn);
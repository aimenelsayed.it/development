import React from 'react';
import './header.styles.scss';
import { ReactComponent as Logo } from '../../assets/logo.svg'
import { connect } from 'react-redux';        
import CartIcon from '../cart-icon/cart-icon.component';    
import CartDropdown from '../cart-dropdown/cart-dropdown.component'; 
import { createStructuredSelector } from 'reselect';
import { selectCartHidden } from '../../redux/cart/cart.selectors';
import { selectCurrentUser } from '../../redux/user/user.selectors';
import { HeaderContainer, LogoContainer, OptionsContainer,OptionLink, OptionDiv } from './header.styles'
import { signOutStart } from '../../redux/user/user.actions';




const Header = ({ currentUser, hidden, signOutStart }) => (
    <HeaderContainer>
       <LogoContainer to="/"> <Logo className='logo'/></LogoContainer>
       <OptionsContainer className='options'>
           <OptionLink  to='/shop'>SHOP</OptionLink>
           <OptionLink  to='/voucher'>VOUCHER</OptionLink>
           <OptionLink to='/contact'>CONTACT</OptionLink>
           {
               currentUser ?
               <OptionLink  as='div'  onClick={signOutStart}>SIGN <span style={{ color: 'green' }}>OUT</span></OptionLink> :
               <OptionLink  to='/signin' >SIGN <span style={{ color: 'red' }}>IN</span> </OptionLink>
           }
           <CartIcon></CartIcon>
           </OptionsContainer>
           {
               hidden ? null : <CartDropdown/>
           }
    </HeaderContainer>
);

/* without sectore we must ,,,,, use state >>> state.user.currentUser */
const mapStateToProps =  createStructuredSelector ({
 currentUser: selectCurrentUser,
 hidden: selectCartHidden
});

/* to make sure rootreudcer to listen*/
/* redux use dispatch to send alllll object to reducer */
const mapDispatchToProps = dispatch  => ({
    signOutStart: () => dispatch(signOutStart())
});

/*  mapStateToProps to acess state from root reducer  */
export default connect(mapStateToProps, mapDispatchToProps)(Header);
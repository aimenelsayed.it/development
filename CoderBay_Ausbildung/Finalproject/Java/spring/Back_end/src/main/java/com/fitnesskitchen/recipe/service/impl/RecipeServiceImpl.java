package com.fitnesskitchen.recipe.service.impl;

import com.fitnesskitchen.recipe.io.entity.RecipeEntity;
import com.fitnesskitchen.recipe.io.entity.UserEntity;
import com.fitnesskitchen.recipe.io.repositories.RecipeRepository;
import com.fitnesskitchen.recipe.io.repositories.UserRepository;
import com.fitnesskitchen.recipe.service.RecipeService;
import com.fitnesskitchen.recipe.shared.dto.RecipeDTO;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
@Service
public class RecipeServiceImpl implements RecipeService {


    @Autowired
    UserRepository userRepository;

    @Autowired
    RecipeRepository recipeRepository;

////get user by idddd
    @Override
    public List<RecipeDTO> getRecipes(String userId) {
             List<RecipeDTO> returnValue = new ArrayList<>();
              ModelMapper modelMapper = new ModelMapper();

        UserEntity userEntity = userRepository.findByUserId(userId);

        if(userEntity==null) return returnValue;
        //// with recpositr we can fetch data bas
        // ///// IMP <<<<<<<<<<<<<<<<<<<<  i could finad all data by id if i use normail id , but userId here is auto genrate that no ID

        Iterable<RecipeEntity> recipes = recipeRepository.findAllByUserDetails(userEntity);
        for(RecipeEntity recipeEntity:recipes)
        {
            returnValue.add( modelMapper.map(recipeEntity, RecipeDTO.class) );
        }

        return returnValue;





    }
    ////////>>>>>>>>>>>>>>>
    @Override
    public RecipeDTO getRecipe(String recipeId) {
            RecipeDTO returnValue =null;

            /// if there is recipe id will go to mapping and get reuslt
        RecipeEntity recipeEntity = recipeRepository.findByRecipeId(recipeId);

        if(recipeEntity!=null)
        {
            returnValue = new ModelMapper().map(recipeEntity, RecipeDTO.class);
        }

        return returnValue;
    }









}

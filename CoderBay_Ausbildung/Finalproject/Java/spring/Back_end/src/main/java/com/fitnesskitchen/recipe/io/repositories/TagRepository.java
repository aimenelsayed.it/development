package com.fitnesskitchen.recipe.io.repositories;

import com.fitnesskitchen.recipe.io.entity.RecipeEntity;
import com.fitnesskitchen.recipe.io.entity.TagsEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TagRepository extends CrudRepository<TagsEntity, Integer>{

    TagsEntity findByName(String name);
}

package com.fitnesskitchen.recipe.ui.model.response;

import com.fitnesskitchen.recipe.io.entity.TagsEntity;
import com.fitnesskitchen.recipe.shared.dto.UserDto;
import org.springframework.hateoas.RepresentationModel;

import java.util.List;

////>>>>>>>>>>>>>RepresentationModel<RecipesRest>  to add links
// we dont need extends RepresentationModel<RecipesRest> because will chnag to enititymodel
public class RecipesRest extends RepresentationModel<RecipesRest>  {

    private String recipeId;

    private String type;
    private String description;
    private List<TagsEntity> tags;





    /////////get and set............................................
    public String getRecipeId() {
        return recipeId;
    }

    public void setRecipeId(String recipeId) {
        this.recipeId = recipeId;
    }

    public List<TagsEntity> getTags() {
        return tags;
    }

    public void setTags(List<TagsEntity> tags) {
        this.tags = tags;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

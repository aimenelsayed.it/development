package com.fitnesskitchen.recipe.ui.model.response;

public enum RequestOperationStatus {
    ERROR, SUCCESS
}

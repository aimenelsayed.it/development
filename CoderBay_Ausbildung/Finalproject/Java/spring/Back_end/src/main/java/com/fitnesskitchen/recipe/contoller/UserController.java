package com.fitnesskitchen.recipe.contoller;


import com.fitnesskitchen.recipe.service.RecipeService;
import com.fitnesskitchen.recipe.service.UserService;
import com.fitnesskitchen.recipe.shared.dto.RecipeDTO;
import com.fitnesskitchen.recipe.shared.dto.UserDto;
import com.fitnesskitchen.recipe.shared.dto.UserDtoRegister;
import com.fitnesskitchen.recipe.ui.model.request.UserDetailsRequestModel;
import com.fitnesskitchen.recipe.ui.model.request.UserDetailsRequestRegisterModel;
import com.fitnesskitchen.recipe.ui.model.request.UserLoginRequestModel;
import com.fitnesskitchen.recipe.ui.model.response.*;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.mvc.WebMvcAffordanceBuilderDsl;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

@CrossOrigin
// to recive http when they send and match
@RestController
// user url http/customer
@RequestMapping("users") // http://localhost:8080/fitnesskitchen.recipe/users/{id}
public class UserController {
    ///>>>>>

    @Autowired
    UserService userService;
    @Autowired
    RecipeService recipesService;
    @Autowired
    RecipeService recipeService;

///////>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> get maping <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
////>>>>>>MediaType to get h1 (html) response
    @GetMapping(path ="/{id}", produces = { MediaType.APPLICATION_JSON_VALUE})
    public UserRest  getUser(@PathVariable String id){
        UserRest returnValue = new UserRest();
          UserDto userDto = userService.getUserByUserId(id);
          BeanUtils.copyProperties(userDto, returnValue);

          return returnValue;
    }





//----------------------------------post section----------------------------------------<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    //>>>>>>>>>>>> @Requestbody to read what will come from site
    @PostMapping(
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})

    ////// to acept request body @RequestBody UserDetailsRequestModel userDetails
    public UserRest createUser(@RequestBody UserDetailsRequestModel userDetails) throws Exception
    {
        //>>>>>>>>>>>>>>>>>>> UserRest:  return date
        UserRest returnValue = new UserRest();
 //>>>will cehck for required faild if empty

        if(userDetails.getFirstName().isEmpty()) throw new NullPointerException("The object is null");
        // UserDto userDto = new UserDto();
         ///>>>>>>>>>> BeanUtils:   copy date from user to target
        //  BeanUtils.copyProperties(userDetails, userDto);

         //// map from user detalis to Dto then database
        ModelMapper modelMapper = new ModelMapper();
        UserDto userDto = modelMapper.map(userDetails, UserDto.class);

      ///>>>>>>>     service will add some info to userDto and return all to userRest
       // UserDto createdUser = userService.createUser(userDto);
       // BeanUtils.copyProperties(createdUser, returnValue);

        System.out.println(userDto);

        UserDto createdUser = userService.createUser(userDto);
        returnValue = modelMapper.map(createdUser, UserRest.class);



        return returnValue;


    }
////// --------------POST--------------------------------------------


    @PostMapping(path ="/register",
            consumes = { MediaType.APPLICATION_JSON_VALUE},
            produces = { MediaType.APPLICATION_JSON_VALUE})
    public UserRestRegister createUser(@RequestBody UserDetailsRequestRegisterModel userDetails) throws Exception
    {
        UserRestRegister returnValue = new UserRestRegister();
        if(userDetails.getFirstName().isEmpty()) throw new NullPointerException("The object is null");
         UserDtoRegister userDtoRegister = new UserDtoRegister();
          BeanUtils.copyProperties(userDetails, userDtoRegister);
        ///>>>>>>>     service will add some info to userDto and return all to userRest
         UserDtoRegister createdUser = userService.createUser(userDtoRegister);
         BeanUtils.copyProperties(createdUser, returnValue);
        return returnValue;


    }
/////// login



    /// get all users to fetch










    //----------------------------------update section----------------------------------------
     @PutMapping(path ="/{id}",
             consumes = { MediaType.APPLICATION_JSON_VALUE},
             produces = { MediaType.APPLICATION_JSON_VALUE})
     //>>> 2 arument 1 to read id other , user detail we want update
    public UserRest updateUser(@PathVariable String id, @RequestBody UserDetailsRequestModel userDetails){
         UserRest returnValue = new UserRest();
         if(userDetails.getFirstName().isEmpty()) throw new NullPointerException("The object is null");
         UserDto userDto = new UserDto();
         BeanUtils.copyProperties(userDetails, userDto);
         ///// insted from create , update >>>>  userService.createUser
         UserDto updatedUser = userService.updateUser(id, userDto);
         BeanUtils.copyProperties(updatedUser, returnValue);
         return returnValue;
    }











    //----------------------------------delete section----------------------------------------<<<<<<<<<<<<<<<<<<

    @DeleteMapping(path ="/{id}",
            produces = { MediaType.APPLICATION_JSON_VALUE})

    public OperationStatusModel deleteUser(@PathVariable String id)
    {
        OperationStatusModel returnValue = new OperationStatusModel();
        returnValue.setOperationName(RequestOperationName.DELETE.name());
        userService.deleteUser((id));
        returnValue.setOperationName(RequestOperationStatus.SUCCESS.name());
        return  returnValue;
    }

    //////// to map API >>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
    public List<UserRest> getUsers(@RequestParam(value="Page", defaultValue = "0") int page,
                                   @RequestParam(value="limit", defaultValue = "25") int limit)
    {  /// we need list becaue wil back alot not 1
        List<UserRest> returnValue = new ArrayList<>();
        List<UserDto> users = userService.getUsers(page,limit);

        for (UserDto userDto : users) {
            UserRest userModel = new UserRest();
            BeanUtils.copyProperties(userDto, userModel);
            returnValue.add(userModel);
        }
        return returnValue;

    }



    ///////////><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<get list of recipe for specif user >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><
///>>>>>>>>> http://localhost:8080/ fsfsf/users_id/recipes
    @GetMapping(path ="/{id}/recipes", produces = { MediaType.APPLICATION_JSON_VALUE})
    public CollectionModel<RecipesRest>  getUserRecipes(@PathVariable String id){
        List<RecipesRest> returnValue = new ArrayList<>();

        List<RecipeDTO> recipesDTO = recipesService.getRecipes(id);
          if(recipesDTO != null && !recipesDTO.isEmpty()){
              Type listType = new TypeToken<List<RecipesRest>>() {}.getType();
              returnValue = new ModelMapper().map(recipesDTO, listType);
          }

          //// for loop so each recipe have own link
    for(RecipesRest recipesRest : returnValue){

        Link selfLink = WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(UserController.class)
                .getUserRecipe(id, recipesRest.getRecipeId())).withSelfRel();
        recipesRest.add(selfLink);

    }


        Link userLink = WebMvcLinkBuilder.linkTo(UserController.class).slash(id).withRel("Profile");
        Link selfLink = WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(UserController.class)
                .getUserRecipes(id)).withSelfRel();
        // .slash(userId).slash("recipes")
        // .slash(recipeId)
        return CollectionModel.of(returnValue,userLink, selfLink );
    }







 ////////////   get map sigle addres detailssss >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    @GetMapping(path ="/{userId}/recipes/{recipeId}",
            produces = { MediaType.APPLICATION_JSON_VALUE})
    //retuen
    public EntityModel<RecipesRest>  getUserRecipe(@PathVariable String userId,  @PathVariable String recipeId) {

        RecipeDTO recipeDTO = recipeService.getRecipe(recipeId);
         ModelMapper modelMapper = new ModelMapper();
        RecipesRest returnValue = modelMapper.map(recipeDTO, RecipesRest.class);
         //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Create link >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
       Link userLink = WebMvcLinkBuilder.linkTo(UserController.class).slash(userId).withRel("Profile");
       Link userRecipesLink = WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(UserController.class).getUserRecipes(userId))
               //.slash(userId).slash("recipes")
               .withRel("recipes");
        //http://localhost:8080/users/<userId> >>>>>>>>>>> to  userLink
        //http://localhost:8080/users/<userId>/recipes>>>>>>>>>>>>>>> to  userRecipesLink

        ///create 3rd link
        //http://localhost:8080/users/<userId>/recipes/recipeId>>>>>>>>>>>>>>> to  selfLink
        Link selfLink = WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(UserController.class)
                .getUserRecipe(userId,recipeId)).withSelfRel();
               // .slash(userId).slash("recipes")
               // .slash(recipeId)


        //returnValue.add(userLink);
        //returnValue.add(userRecipesLink);
        //returnValue.add(selfLink);

        ////Entity model

        return   EntityModel.of(returnValue, Arrays.asList(userLink,userRecipesLink,selfLink));
    }







}

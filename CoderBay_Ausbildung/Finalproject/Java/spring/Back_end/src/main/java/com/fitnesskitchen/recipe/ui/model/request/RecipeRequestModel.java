package com.fitnesskitchen.recipe.ui.model.request;

import com.fitnesskitchen.recipe.io.entity.TagsEntity;

import java.util.List;

// create to Map recipe
public class RecipeRequestModel {

    private String type;
    private String description;
    private List<TagsEntity> tags;



//////////// set and get...................................


    public List<TagsEntity> getTags() {
        return tags;
    }

    public void setTags(List<TagsEntity> tags) {
        this.tags = tags;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

package com.fitnesskitchen.recipe;

import com.fitnesskitchen.recipe.security.AppProperties;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;


@SpringBootApplication
public class FitnessKitchenRecipeApplication
		extends SpringBootServletInitializer
{

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(FitnessKitchenRecipeApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(FitnessKitchenRecipeApplication.class, args);
	}

	///>>>>  to make incript password
	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}

	//// to be apple to acees it
	@Bean
	public SpringApplicationContext springApplicationContext(){
		return  new SpringApplicationContext();
	}

	@Bean(name="AppProperties")
	public AppProperties getAppProperties()
	{
		return  new AppProperties();
	}
}

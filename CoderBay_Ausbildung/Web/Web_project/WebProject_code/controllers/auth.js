const mysql = require("mysql")
const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs')
const {promisify} = require ('util');
const bodyparser = require('body-parser');
const express = require('express')



const app = express()


app.use(bodyparser.json());
app.use(bodyparser.urlencoded({extended: true}));

/*const db = mysql.createConnection({
    host : process.env.DaTABASE_HOST,
    user: process.env.DaTABASE_USER,
    password: process.env.DaTABASE_password,
    database: process.env.DaTABASE,

    

})*/


var db = mysql.createConnection({
    host: 'localhost',
    database: 'genshin',
    user: 'root',
    password: ''
});
///////////----------------Login--------------
exports.login = async (req, res) => {
  try {
      const {email, password} = req.body;

      if(!email || !password ) {
          return res.status(400).render('login', {
              message: 'put both email and password DUDE!!!!'
          })
      }

      db.query('SELECT * FROM genshin WHERE email = ?' , [email], async(error, results) =>{
     /////////------------check login  input ------------------------     
        if (!results || !(await bcrypt.compare(password, results[0].password))) {
            return res.status(400).render('login', {
                message: 'Email or password incorrect'
            })
        }else {
            const id = results[0].id;
    ///////////////----------Make hot cockkkkkkkkiiiesesses------------
            const token = jwt.sign({id: id}, process.env.JWT_SECRET, {
                expiresIn: process.env.JWT_EXPIRES_IN
            })
            console.log("THE TOKEN IS : " + token)
            const cookieOptions = {
                expires: new Date(
                    Date.now() + process.env.JWT_COOKIE_EXPIRS * 24 * 60 * 60 * 1000
                ),
                httpOnly: true 
            }
            

             res.cookie('JWT', token, cookieOptions);
             res.status(200).redirect("/profile")


        }
     })

     

  } catch (error) {
     console.log('login in Error: ' + error) 
  }
}
/////////////////-----------------------Register--------------
exports.register = (req, res) => {
    
    console.log(req.body)
  
     const {name, email, password, passwordConfirm} = req.body;
      
///////////////////-------------database connect-------------
     db.query('SElECT email FROM genshin WHERE email=?', [email], async (error, result) => {
         if(error) {
             console.log(error)
         }

         if(result.length > 0) {
             return res.render('register', {
                 message: 'That email is ready on use'
             })
         } else if ( password !== passwordConfirm) {
            return res.render('register', {
                message: 'Password not match'
            })
         }

////////////////////-------------------  hash pasword----------      
            let hashedPassword = await bcrypt.hash(password, 8)
            console.log(hashedPassword)
//////////////-----------database add--------------------

            db.query('INSERT INTO genshin SET ?', {name: name, email: email, password: hashedPassword}, (error, result)=>{
                if(error) {
                    console.log(error)
                } else {

                    console.log(result)
                    return res.render('register', {
                        message: 'Player Registerd'
                    })
                }
                

            })

      })

   
}

/////////////////---------------- control profile ----------

exports.isLoggedIN = async (req, res, next) => {
     console.log(req.cookies.JWT)
    if( req.cookies.JWT) {
      try {
        //1) verify the token
        const decoded = await promisify(jwt.verify)(req.cookies.JWT,
        process.env.JWT_SECRET
        );
  
        console.log(decoded);
  
        //2) Check if the user still exists
        db.query('SELECT * FROM genshin WHERE id = ?', [decoded.id], (error, result) => {
          console.log(result, error);
  
          if(!result) {
            return next();
          }
  
          req.user = result[0];
          console.log("user is")
          console.log(req.user);
          return next();
  
        });
      } catch (error) {
        console.log(error);
        return next();
      }
    } else {
      next();
    }
  }

//////////////////////-------------------logout-----------



exports.logout = async (req, res) => {
    res.cookie('JWT', 'logout', {
        expires: new Date(Date.now() + 2*1000 ),
        htppOnly: true
    })

   res.status(200).redirect('/login')

}
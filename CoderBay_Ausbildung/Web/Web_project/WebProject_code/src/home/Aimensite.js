
/* ...................................splash ....................................................*/
 const splash = document.querySelector('.splash')
 document.addEventListener('DOMContentLoaded', (e) => {
     setTimeout(() =>{
         splash.classList.add('display-none')
     }, 3000)
 })







   /*--------------------------------------- slide show.....................................................*/
const panels = document.querySelectorAll('.panel')

panels.forEach(panel => {
    panel.addEventListener('click', () => {
        removeActiveClasses()
        panel.classList.add('active')
    })
})

function removeActiveClasses() {
    panels.forEach(panel => {
        panel.classList.remove('active')
    })
}

       /*--------------------------------------- nav.....................................................*/


function openNav() {
  document.getElementById("mySidenav").style.width = "250px";
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
}

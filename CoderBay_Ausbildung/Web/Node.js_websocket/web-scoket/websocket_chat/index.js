const express = require('express')
const WebSocket = require('ws')
var path = require('path')
//const server = require('http').createServer(app);



const app = express()
//app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'ejs')
app.use(express.static('websocket_chat'))

app.get('/', function (req, res) {
res.sendFile(path.join(__dirname,'/views/index.html'))
})

app.get('/2', function (req, res) {
    res.sendFile(path.join(__dirname,'/views/index2.html'))
    })


const wss = new WebSocket.Server({port: 8080})

wss.on('connection', (ws) => { 
    console.log('we have new client')
    ws.send('Welcome to Aimen chat')
    ws.on('message', (message) => {
        console.log(`Received message => ${message}`)

        wss.clients.forEach(function each(client){
            if (client !== ws && client.readyState === WebSocket.OPEN) {
                client.send(message);
            }
        })
        /*let msg = JSON.parse(message)

        if (msg.joinRoom) {
            ws.room = msg.joinRoom
        } 
            websocketSendToAll(message)
        }*/
    })
    //ws.send(JSON.stringify({message: 'Hello! Message From Server!!'}))
})



function websocketSendToAll(text) {
    wss.clients.forEach(function each(client) {
        if (client.readyState === WebSocket.OPEN) {
            if (client.room === JSON.parse(text).room) {
                client.send(text);
            }
        }
    });
}




app.listen(3000, ()=>{
    console.log('server start on port 3000.')
})
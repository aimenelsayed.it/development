package decorator;

public class PizzaShop {
    public static void main(String[] args) {
        Pizza pizza = new CheesePizzaDecorator(new PlanPizza()) ;

        pizza.back();
    }

}

package decorator;

public class PizzaDecrator implements Pizza{

    private Pizza pizza;

    public PizzaDecrator(Pizza pizza) {
        this.pizza = pizza;
    }

    @Override
    public void back() {
           pizza.back();
    }
}

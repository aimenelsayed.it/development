package decorator;

public class CheesePizzaDecorator extends PizzaDecrator{


    public CheesePizzaDecorator(Pizza pizza) {
        super(pizza);
    }

    public void back(){
        super.back();
        System.out.println("add Cheeesee");
    }


}

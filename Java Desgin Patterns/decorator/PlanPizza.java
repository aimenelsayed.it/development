package decorator;

public class PlanPizza implements Pizza{
    @Override
    public void back() {
        System.out.println("Backing normal pizza");
    }
}

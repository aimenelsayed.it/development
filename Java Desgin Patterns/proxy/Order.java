package proxy;

public class Order {


  private  int id;
  private String productName;
  private  int quanttiy;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getQuanttiy() {
        return quanttiy;
    }

    public void setQuanttiy(int quanttiy) {
        this.quanttiy = quanttiy;
    }
}

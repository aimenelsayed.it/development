package abstractfactroy;

public class Test {

    public static void main(String[] args) {

      DaoAbstractFactory daf =  DaoProducer.produce("xml");
      Dao dao = daf.creatDao("emp");
      dao.save();
    }
}

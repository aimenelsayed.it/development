package abstractfactroy;

public class DaoProducer {


       public static DaoAbstractFactory produce(String factorytype){
           DaoAbstractFactory daf = null;
           if(factorytype.equals("xml")){
               daf = new XMLDaoFactory();
           } else if (factorytype.equals("db")) {
               daf = new DBDaoFactory();
           }


           return daf;

       }
}

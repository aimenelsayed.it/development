package abstractfactroy;

public class DBDaoFactory extends DaoAbstractFactory {
    @Override
    public Dao creatDao(String type) {
          Dao  dao = null;
        if(type.equals("emp")){
            dao = new DBEmpDao();
        }
        else if (type.equals("dept"))
        {
            dao = new DBDDeptDao();
        }

        return dao;
    }
}

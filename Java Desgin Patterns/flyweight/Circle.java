package flyweight;

public class Circle extends Shape{

   private String label;




    @Override
    public void draw(int radius, String fillColor, String lineColor) {
        System.out.println("Drawing a " + label + " with raduis " + radius + " fill color " + fillColor + " line color " + lineColor );
    }

    public Circle() {
        this.label = "Circle";
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }



}

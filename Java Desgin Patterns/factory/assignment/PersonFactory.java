package factory.assignment;

public class PersonFactory {

    public  static Person wish(String msg){
        Person p = null;
        if(msg.equals("male")){
            p = new Male();
        }
        else if(msg.equals("female")){
            p = new Female();
        }

        return  p;
    }

}

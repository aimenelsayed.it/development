package factory.assignment;

public class PersonOrder {

    public Person wish(String wish) {
        Person p = PersonFactory.wish(wish);
        p.wish();
        return p;
    }
}

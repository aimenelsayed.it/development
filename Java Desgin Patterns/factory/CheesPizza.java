package factory;

public class CheesPizza  implements  Pizza{
    @Override
    public void prepare() {
        System.out.println("Preaparing Chesse Pizza");
    }

    @Override
    public void bake() {
        System.out.println("Backing Chesse Pizza");

    }

    @Override
    public void cut() {
        System.out.println("Cutting Chesse Pizza");

    }
}

package factory;

public class ChickenPizza implements  Pizza{
    @Override
    public void prepare() {
        System.out.println("Preaparing Chiecken Pizza");
    }

    @Override
    public void bake() {
        System.out.println("Backing Chiecken Pizza");

    }

    @Override
    public void cut() {
        System.out.println("Cutting Chiecken Pizza");

    }
}

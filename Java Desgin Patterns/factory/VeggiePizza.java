package factory;

public class VeggiePizza implements  Pizza{
    @Override
    public void prepare() {
        System.out.println("Preaparing Veggie Pizza");
    }

    @Override
    public void bake() {
        System.out.println("Backing Veggie Pizza");

    }

    @Override
    public void cut() {
        System.out.println("Cutting Veggie Pizza");

    }
}

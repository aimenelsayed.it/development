package template.assignement;

public class DesktopManufacturer extends BuildComputer {
    @Override
    public String addHardDisk() {
        System.out.println("Buliding Desktop");
        return "HardDisk";
    }

    @Override
    public String addRAM() {
        System.out.println("Buliding Desktop");
        return "RAM";
    }

    @Override
    public String addKeyboard() {
        System.out.println("Buliding Desktop");
        return "add keyboard";
    }
}

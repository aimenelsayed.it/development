package template.assignement;

public abstract class BuildComputer {

    public void buidl(){
        addHardDisk();
        addRAM();
        addKeyboard();
        System.out.println("Build In processs");
    }

    public abstract String addHardDisk();
    public abstract String addRAM();
    public abstract String addKeyboard();
}

package adapter.assignment;

public interface PaymentProcessor {

    int pay(String dollars);
}

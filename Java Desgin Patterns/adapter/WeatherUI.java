package adapter;

public class WeatherUI {
    public void showTemprature(int zipcode){
        WeatherAdapter adapter = new WeatherAdapter();
        System.out.println(adapter.findTemparture(zipcode));
    }


    public static void main(String[] args) {
        WeatherUI ui = new WeatherUI();
        ui.showTemprature(19406);


    }}
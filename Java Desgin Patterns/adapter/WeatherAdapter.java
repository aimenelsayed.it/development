package adapter;

public class WeatherAdapter {

    public int findTemparture(int zipcode){

        String city = null;
        if (zipcode == 19406){
            city = "King of Prussi";}

        WeatherFinder finder = new WeatherFinderImpl();
        int temprature = finder.find(city);
        return temprature;
    }
}

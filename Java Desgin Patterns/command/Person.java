package command;

public class Person {
    public static void main(String[] args) {
        Television television = new Television();
        RemtoControl remtoControl = new RemtoControl();

        OnCommand onCommand = new OnCommand(television);
        remtoControl.setCommand(onCommand);
        remtoControl.pressButtom();


        OffCommand offCommand = new OffCommand(television);
        remtoControl.setCommand(offCommand);
        remtoControl.pressButtom();
    }
}

package command;

public class Television {
    public void on(){
        System.out.println("TV Switch on");
    }

    public void off(){
        System.out.println("TV Switch off");
    }
}

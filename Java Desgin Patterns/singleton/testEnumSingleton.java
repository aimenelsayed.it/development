package singleton;

import java.net.StandardSocketOptions;

public class testEnumSingleton {

    public static void main(String[] args) {
        EnumSingletonDemo instance = EnumSingletonDemo.INSTANCE;
        System.out.println(instance.getName());
        instance.setName("Aimen");
        System.out.println(instance.getName());
    }
}

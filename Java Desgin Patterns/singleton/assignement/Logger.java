package singleton.assignement;

import java.io.Serializable;

public class Logger implements Serializable, Cloneable {

  private static volatile Logger instance;


    private Logger() {}

    private static Logger getInstance(){
        if (instance == null){
            synchronized(Logger.class){
                if (instance == null){
                    instance = new Logger();
                }
            }
        }
        return instance;
    }



    public void log(String message){
        System.out.println(message);
    }

    private Object readResolve(){
        return instance;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}

package singleton;

import java.io.*;

public class test {
    public static void main(String[] args) throws IOException, ClassNotFoundException {

        DateUtil dateUtil1 = DateUtil.getInstance();
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(new File("/C:/Users/aimen/Documents/singleton/dataUtil.ser")));
        oos.writeObject(dateUtil1);

        DateUtil dateUtil2 = null;
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream(new File("/C:/Users/aimen/Documents/singleton/dataUtil.ser")));
        dateUtil2= (DateUtil) ois.readObject();

        oos.close();
        ois.close();


        System.out.println(dateUtil1==dateUtil2);


    }
}
